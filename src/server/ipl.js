
// matches per year
function matchesPerYear(matches) {
    let result = matches.reduce((result, match) => {
        // extracting year from date
        let year = match.date.split("-")[0];
        
        if ( result.hasOwnProperty( year) === false) {
            result[year] = 1;
        } else {
            result[year]++;
        }
        
        return result;
    }, {})

    return result;
}


// Number of matches won per team per year in IPL.
function matchesPerTeam( matches) {
    let result = {};

    result = matches.reduce(( result, match) => {
        let year = match.date.split("-")[0];

        if ( match.winner.length !== 0) {    
            // create a new team entry
            if (result.hasOwnProperty(match.winner) === false) {
                result[match.winner] = {};
            }

            // create a new date entry
            if (result[match.winner].hasOwnProperty(year) === false) {
                result[match.winner][year] = 1;
            } else {
                result[match.winner][year]++;
            }
        }
        
        return result;
    }, {});

    return result;
}

// helper function to get the match id's with the required year
function getMatchIdsOfYear(matches, requiredYear) {
    let matchIdsOfYear = new Set();

    matchIdsOfYear = matches.reduce((matchIdsOfYear, match) => {
        let year = match.date.split("-")[0];
        
        // year is a String and requiredYear is a Number
        if ( year == requiredYear) {
            matchIdsOfYear.add(match.id);
        }

        return matchIdsOfYear;
    }, new Set());

    return matchIdsOfYear;
}

// Extra runs conceded per team in the year 2016
function extraRunsPerTeam(matches, deliveries,requiredYear) {
    // get the match ids of the specified year
    let matchIdsOfYear = getMatchIdsOfYear(matches, requiredYear);
    
    let result = {};

    result = deliveries.reduce((result, deliverie) => {
        if ( matchIdsOfYear.has(deliverie.match_id) === true) {
            if ( result.hasOwnProperty(deliverie.bowling_team) === false) {
                result[deliverie.bowling_team] = 0;
            }

            result[deliverie.bowling_team] += Number(deliverie.extra_runs);
        }

        return result;
    }, {});
    return result;
}


// Top 10 economical bowlers in the year 2015
function economicalBowlers(matches, deliveries, requiredYear, numberOfTopBowlers) {
    // get the match ids of the specified year
    let matchIdsOfYear = getMatchIdsOfYear(matches, requiredYear);

    let totalRunsEachBowler = {};

    totalRunsEachBowler = deliveries.reduce((totalRunsEachBowler, deliverie) => {
        // match took place in specified year
        if ( matchIdsOfYear.has(deliverie.match_id) === true) {
            if ( totalRunsEachBowler.hasOwnProperty(deliverie.bowler) === false) {
                totalRunsEachBowler[deliverie.bowler] = 0;
            }

            totalRunsEachBowler[deliverie.bowler] += Number(deliverie.total_runs);
        }

        return totalRunsEachBowler;
    }, {});


    // below variable will hold a data structure like this
    //  'S Nadeem': { '539': Set { '8', '12', '14' }, '565': Set { '1', '3', '5', '7' } },
    // 'V Kohli': { '540': Set { '10' }, '554': Set { '12' } }
    let bowlersMatchIdsAndOvers = {};

    bowlersMatchIdsAndOvers = deliveries.reduce((bowlersMatchIdsAndOvers, deliverie) => {
        // match took place in the required year
        if ( matchIdsOfYear.has(deliverie.match_id) === true) {
            if ( bowlersMatchIdsAndOvers.hasOwnProperty(deliverie.bowler) === false) {
                bowlersMatchIdsAndOvers[deliverie.bowler] = {};
            }
    
            if ( bowlersMatchIdsAndOvers[deliverie.bowler].hasOwnProperty(deliverie.match_id) === false) {
                bowlersMatchIdsAndOvers[deliverie.bowler][deliverie.match_id] = new Set();
            }

            bowlersMatchIdsAndOvers[deliverie.bowler][deliverie.match_id].add(deliverie.over);
        }
        
        return bowlersMatchIdsAndOvers;
    }, {});


    let totalOversByBowlers = {};

    totalOversByBowlers = Object.keys(bowlersMatchIdsAndOvers).reduce((totalOversByBowlers, bowler) => {
        if ( totalOversByBowlers.hasOwnProperty(bowler) === false) {
            totalOversByBowlers[bowler] = 0;
        }
        
        Object.keys(bowlersMatchIdsAndOvers[bowler]).reduce((_, match_id) => {
            totalOversByBowlers[bowler] += bowlersMatchIdsAndOvers[bowler][match_id].size;

            return _;
        }, {});

        return totalOversByBowlers;
    }, {});


    let economyOfBowlers = {};

    economyOfBowlers = Object.keys(totalRunsEachBowler).reduce((economyOfBowlers, bowler) => {
        economyOfBowlers[bowler] = totalRunsEachBowler[bowler] / totalOversByBowlers[bowler];

        return economyOfBowlers;
    }, {});

    let sortedEconomicalBowlers = Object.entries(economyOfBowlers).sort((a,b) => a[1] - b[1]);

    return sortedEconomicalBowlers.slice(0,numberOfTopBowlers);
}

// -----------------  EXTRA QUESTIONS SECTION  ------------------------
// ****  There is a mistake in the "matches" dataset where Rising Pune Supergiants has
// ****  been duplicated as "Supergiants" and "Supergiant"

// Find the number of times each team won the toss and also won the match
function winTossAndMatch(matches) {
    let countEachTeam = {};

    countEachTeam = matches.reduce((countEachTeam, match) => {
        // there can be a team which never won a toss or a match
        // so adding each team that ever participated in a match
        if ( countEachTeam.hasOwnProperty(match.team1) === false) {
            countEachTeam[match.team1] = 0;
        }
        
        if ( countEachTeam.hasOwnProperty(match.team2) === false) {
            countEachTeam[match.team2] = 0;
        }

        if ( match.toss_winner === match.winner) {
            countEachTeam[match.toss_winner] += 1;
        }

        return countEachTeam;
    }, {});

    return countEachTeam;
}


// Find a player who has won the highest number of Player of the Match awards for each season

function highestNumberOfPlayerMatchSeason(matches) {
    let playerOfMatchEachSeason = {};

    playerOfMatchEachSeason = matches.reduce((playerOfMatchEachSeason, match) => {
        let year = match.date.split("-")[0];

        // there exists a player of match
        if ( match.player_of_match.length !== 0) {
            if ( playerOfMatchEachSeason.hasOwnProperty(year) === false) {
                playerOfMatchEachSeason[year] = {};
            }
    
            if ( playerOfMatchEachSeason[year].hasOwnProperty(match.player_of_match) === false) {
                playerOfMatchEachSeason[year][match.player_of_match] = 1;
            } else {
                playerOfMatchEachSeason[year][match.player_of_match] += 1;
            }
        }
        
        return playerOfMatchEachSeason;
    }, {});

    // there can be more than one player with the highest number of awards
    let result = {};

    result = Object.keys(playerOfMatchEachSeason).reduce((result, season) => {
        let max = Math.max(...Object.values(playerOfMatchEachSeason[season]));

        let playersOfTheSeason = {};

        playersOfTheSeason = Object.keys(playerOfMatchEachSeason[season]).reduce((playersOfTheSeason, player) => {
            if(playerOfMatchEachSeason[season][player] === max) {
                playersOfTheSeason[player] = playerOfMatchEachSeason[season][player];
            }
            
            return playersOfTheSeason;
        }, {});

        result[season] = playersOfTheSeason;

        return result;
    }, {});

    return result;
}

// helper function to get all match id with corresponding year
function getAllMatchId(matches) {
    let result = {};

    result = matches.reduce((result, match) => {
        let year = match.date.split("-")[0];
    
        result[match.id] = year;

        return result;
    }, {});

    return result;
}


// Find the strike rate of a batsman for each season
function strikeRateEachBatsmanPerSeason(matches, deliveries) {
    /*
    {
        "apple": { 2008:[125,20], 2009:[10,2]},
        "orange": { 2010: [300, 100], 2011:[0,0]}
    }
    */
    let matchIdWithYear = getAllMatchId(matches);

    let strikeRateOfBatsmanEachSeason = {};

    strikeRateOfBatsmanEachSeason = deliveries.reduce((strikeRateOfBatsmanEachSeason, deliverie) => {
        if ( strikeRateOfBatsmanEachSeason.hasOwnProperty(deliverie.batsman) === false) {
            strikeRateOfBatsmanEachSeason[deliverie.batsman] = {};
        }

        let year = matchIdWithYear[deliverie.match_id];

        if ( strikeRateOfBatsmanEachSeason[deliverie.batsman].hasOwnProperty(year) === false) {
            // [0,0] => will track runs and balls faced
            strikeRateOfBatsmanEachSeason[deliverie.batsman][year] = [0,0];
        }

        strikeRateOfBatsmanEachSeason[deliverie.batsman][year][0] += isNaN(deliverie.total_runs) ? 0 : Number(deliverie.total_runs);
        strikeRateOfBatsmanEachSeason[deliverie.batsman][year][1] += 1;

        return strikeRateOfBatsmanEachSeason;
    }, {});

    Object.keys(strikeRateOfBatsmanEachSeason).reduce((strikeRateOfBatsmanEachSeason,striker) => {
        Object.keys(strikeRateOfBatsmanEachSeason[striker]).reduce((strikeRateOfBatsmanEachSeason,year) => {
            let totalRuns = strikeRateOfBatsmanEachSeason[striker][year][0];
            let totalBalls = strikeRateOfBatsmanEachSeason[striker][year][1];
            
            strikeRateOfBatsmanEachSeason[striker][year] = (totalRuns / totalBalls) * 100;
            
            return strikeRateOfBatsmanEachSeason;
        }, strikeRateOfBatsmanEachSeason);

        return strikeRateOfBatsmanEachSeason;
    }, strikeRateOfBatsmanEachSeason);

    return strikeRateOfBatsmanEachSeason;
}


// Find the highest number of times one player has been dismissed by another player
function highestDismissal(deliveries) {
    // {
    //     "bowler1": {"batsman1":2, "batsman2":1},
    //     "bowler2": {"batsman3":5, "batsman1":1} 
    // }
    let batsmanDismissedByBowlers = {};

    batsmanDismissedByBowlers = deliveries.reduce((batsmanDismissedByBowlers, deliverie) => {
        // batsman is dismissed by this deliverie
        if ( deliverie.player_dismissed.length !== 0) {
            if ( batsmanDismissedByBowlers.hasOwnProperty(deliverie.bowler) === false) {
                batsmanDismissedByBowlers[deliverie.bowler] = {};
            }
    
            if ( batsmanDismissedByBowlers[deliverie.bowler].hasOwnProperty(deliverie.player_dismissed) === false) {
                batsmanDismissedByBowlers[deliverie.bowler][deliverie.player_dismissed] = 1;
            } else {
                batsmanDismissedByBowlers[deliverie.bowler][deliverie.player_dismissed]++;
            }
        }
        
        return batsmanDismissedByBowlers;
    }, {});

    let max = Object.keys(batsmanDismissedByBowlers).reduce((max, bowler) => {
        let maxByBowler = Math.max(...Object.values(batsmanDismissedByBowlers[bowler]));
        
        max = Math.max(maxByBowler, max);

        return max;
    },0);

    let result = Object.keys(batsmanDismissedByBowlers).reduce((result, bowler) => {
        
        let bowlerWithMaxDissmisals = Object.keys(batsmanDismissedByBowlers[bowler]).reduce((bowlerWithMaxDissmisals, batsman) => {
            if ( batsmanDismissedByBowlers[bowler][batsman] === max) {
                if ( bowlerWithMaxDissmisals.hasOwnProperty(bowler) === false) {
                    bowlerWithMaxDissmisals[bowler] = [];
                }
                bowlerWithMaxDissmisals[bowler].push(batsman,batsmanDismissedByBowlers[bowler][batsman]);
            }

            return bowlerWithMaxDissmisals;
        }, {});
        
        if ( bowlerWithMaxDissmisals.hasOwnProperty(bowler)) {
            result[bowler] = bowlerWithMaxDissmisals[bowler];
        }

        return result;
    },{});
    
    return result;
}

// Find the bowler with the best economy in super overs
function bestSuperOverEconomical(deliveries) {
    /*
        {
            "apple": [ Set {34, 56}, 20],
            "orange": [ Set {12,4}, 35]
        }
        Set will have match ids
        2nd element in array is total runs
    */

    let bowlerOversAndRuns = {};

    bowlerOversAndRuns = deliveries.reduce((bowlerOversAndRuns, deliverie) => {
            if ( deliverie.is_super_over == 1) {
                if ( bowlerOversAndRuns.hasOwnProperty(deliverie.bowler) === false) {
                    bowlerOversAndRuns[deliverie.bowler] = [new Set(), 0];
                }
        
                bowlerOversAndRuns[deliverie.bowler][0].add(deliverie.match_id);
                bowlerOversAndRuns[deliverie.bowler][1] += Number(deliverie.total_runs);
            }
            
            return bowlerOversAndRuns;
    }, {});

    let bowlerStats = [];

    bowlerStats = Object.keys(bowlerOversAndRuns).reduce((bowlerStats, bowler) => {
        bowlerStats.push([bowler, bowlerOversAndRuns[bowler][1] / bowlerOversAndRuns[bowler][0].size]);

        return bowlerStats;
    }, []);
    
    let bestEconomy = Infinity;

    bestEconomy = bowlerStats.reduce((bestEconomy, bowlerStats) => {
        bestEconomy = Math.min(bowlerStats[1], bestEconomy);

        return bestEconomy;
    }, Infinity);

    let result = [];

    result = bowlerStats.filter((bowlerStat) => {
        return bowlerStat[1] === bestEconomy;
    });

    return result;
}

module.exports.matchesPerYear = matchesPerYear;
module.exports.matchesPerTeam = matchesPerTeam;
module.exports.extraRunsPerTeam = extraRunsPerTeam;
module.exports.economicalBowlers = economicalBowlers;
module.exports.winTossAndMatch = winTossAndMatch;
module.exports.highestNumberOfPlayerMatchSeason = highestNumberOfPlayerMatchSeason;
module.exports.highestDismissal = highestDismissal;
module.exports.strikeRateEachBatsmanPerSeason = strikeRateEachBatsmanPerSeason;
module.exports.bestSuperOverEconomical = bestSuperOverEconomical;