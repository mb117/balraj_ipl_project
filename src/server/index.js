const ipl = require('./ipl.js');
const fs = require('fs');
const csvToJson = require('convert-csv-to-json');

let matches = csvToJson.fieldDelimiter(',').getJsonFromCsv("../data/matches.csv");
let deliveries = csvToJson.fieldDelimiter(',').getJsonFromCsv("../data/deliveries.csv");

try {
    fs.writeFileSync('../public/output/matchesPerYear.json', JSON.stringify(ipl.matchesPerYear(matches)));
    fs.writeFileSync('../public/output/matchesPerTeam.json', JSON.stringify(ipl.matchesPerTeam(matches)));
    fs.writeFileSync('../public/output/extraRunsPerTeam.json', JSON.stringify(ipl.extraRunsPerTeam(matches, deliveries, 2016)));
    fs.writeFileSync('../public/output/economicalBowlers.json', JSON.stringify(ipl.economicalBowlers(matches, deliveries, 2015, 10)));
    fs.writeFileSync('../public/output/winTossAndMatch.json', JSON.stringify(ipl.winTossAndMatch(matches)));
    fs.writeFileSync('../public/output/highestNumberOfPlayerMatchSeason.json', JSON.stringify(ipl.highestNumberOfPlayerMatchSeason(matches)));
    fs.writeFileSync('../public/output/highestDismissal.json', JSON.stringify(ipl.highestDismissal(deliveries)));
    fs.writeFileSync('../public/output/strikeRateEachBatsmanPerSeason.json', JSON.stringify(ipl.strikeRateEachBatsmanPerSeason(matches, deliveries)));
    fs.writeFileSync('../public/output/bestSuperOverEconomical.json', JSON.stringify(ipl.bestSuperOverEconomical(deliveries)));
    
    // console.log("JSON data is saved.");
} catch (err) {
    // console.error(err);
}

